@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			{{-- S05 Stretch Goal --}}

			<p>
				<span class="p-2" style="border-right: thin solid;">Likes: {{count($post->likes)}}</span>
				<span class="p-1">Comments: {{count($post->comments)}}</span>
			</p>

			{{-- S05 Stretch Goal --}}
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif					
					</form>
					<form class="d-inline">
						<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">Comment</button>
					</form>
				@endif
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	{{-- Comment Section for s05 activity --}}
	@foreach($post->comments as $comment)
		<div class="card text-center my-2">
			<div class="card-body">
				<p>
					{{$comment->content}}
				</p>

				<h6 class="card-text mb-3">
					Author: {{$comment->user->name}}
				</h6>
				<p class="card-subtitle mb-3 text-muted">Created at : {{$comment->created_at}}</p>
			</div>
			
		</div>
	@endforeach


	{{-- Modal for s05 Activity (Posting comments) --}}

	<div class="modal fade" id="commentModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="staticBackdropLabel">Insert Your Comment</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <form class="p-2" method="POST" action="/posts/{{$post->id}}/comment">
	      		@method('PUT')
	      		@csrf

	      		<div class="form-group">
	      			<label for="content">Content:</label>
	      			<textarea name="content" id="content" class="form-control"></textarea>	
	      		</div>

	      		<div class="modal-footer">
	      		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	      		  <button type="submit" class="btn btn-primary">Comment</button>
	      		</div>
	      	</form>

	    </div>
	  </div>
	</div>
	
@endsection